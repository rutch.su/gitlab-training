# Setup GitLab Project for training

### Login to GitLab.com

### At the top of level create a first group.

### Under the first group, create a new project.

### Manage member permission 
- At the group or project level
- Go to manage menu > member
- Choose a role for your member

## Enable 2 Factor Authentication
- At the group level
- Go to setting menu > General 
- Scroll down to the Two-factor authentication topic
- Click at the box of "All users in this group must set up two-factor authentication" for enable
- Click at Save change


### การสร้าง SSH key 
1. สร้าง SSH key pair จากในเครื่อง client โดยเลือกประเภทจากด้านล่าง
    - 1.1 ED25519 

    - 1.2 RSA

2. เลือกใช้ command ตามด้านล่าง และใส่ข้อมูลให้เรียบร้อย
  #### For example, for ED25519:

``` 
ssh-keygen -t ed25519 -C "<comment>" 
```

  #### For 2048-bit RSA:

```
ssh-keygen -t rsa -b 2048 -C "<comment>"
```
3. นำข้อมูล Key ที่ได้มาไป configure ที่ GitLab 
4.	คลิกที่รูป User profile ด้านขวาบน แล้วเลือก Edit profile
5.	เลือกเมนูด้านซ้ายที่ SSH Keys
6.	ตั้งชื่อ Access Token เลือก scopes ที่ต้องการจะใช้



### การสร้าง Access token 
1.	คลิกที่รูป User profile ด้านขวาบน แล้วเลือก Edit profile
2.	เลือกเมนูด้านซ้ายที่ Access Tokens
3.	ตั้งชื่อ Access Token เลือก scopes ที่ต้องการจะใช้
ในที่นี้เราจะเลือกทุกอันยกเว้น sudo แล้วกด Create
 
4.	จากนั้น COPY token ไปเก็บไว้ที่อื่น

    `หากปิดหน้าต่างหรือเปลี่ยนหน้าแล้วจะไม่สามารถดูได้อีก`
 
